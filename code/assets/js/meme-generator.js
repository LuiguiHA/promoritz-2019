Vue.component('input-autofit', {
  delimiters: ['${', '}'],
  props: {
    fontSize: {
      type: Number,
      default: 11
    },
    fontFamily: {
      type: String,
      default: "Arial"
    },
    width: {
      type: Number,
      default: 100
    },
    text: {
      type: String,
      default: "sdsdsd"
    },
    fontColor: {
      type: String,
      default: "#000000"
    }
  },
  data: function () {
    return {
      calculatedFontSize: ""
    }
  },
  mounted: function(){
    this.calculatedFontSize = this.fontSize
  },
  watch: {
    text: function(newValue){
        this.$nextTick(function(){
            this.resizeContents()        
        }.bind(this))
        
    }
  },
  methods:{
    resizeContents: function(){

        var input = this.$refs.realInput
        var inputWidth = Math.round(input.getBoundingClientRect().width);
        var percent = this.width / inputWidth

        if(percent < 1){
            this.calculatedFontSize = this.fontSize * percent
        } else {
            this.calculatedFontSize = this.fontSize 
            percent = 1
        }

        this.$emit("input", percent)

    }
  },
  template: "#input-autofit"
})

var memeGenerator = new Vue({
  el: "#meme-generator",
  delimiters: ['${', '}'],
  props:{
    finalMemeWidth: {
      type: Number,
      default: 500
    },
    finalMemeHeight: {
      type: Number,
      default: 500
    },
    previewMemeWidth: {
      type: Number,
      default: 200
    },
    previewMemeHeight: {
      type: Number,
      default: 200
    },
    memeFontSize: {
      type: Number,
      default: 11
    }
  },
  data: {
    imageUrl: null,
    colorBottom: "#000000",
    textBottom: "",
    bottomFontSize: 0,
    colorTop: "#000000",
    textTop: "",
    topFontSize: 0,
    previewImageWidth: 0,
    previewImageHeight: 0,
    finalImageWidth: 0,
    finalImageHeight: 0,
    fontFamily: "Arial"
  },
  validations: {
  },
  components: {
  },
  watch: {
  },
  created: function(){
  },
  mounted: function(){
    this.bottomFontSize = this.finalFontSize
  },
  computed: {
    finalFontSize: function(){
        return (this.memeFontSize * ((this.finalMemeWidth / this.previewMemeWidth))) 
    },
  },
  methods:{
    handleTextBottom: function(percent){
        this.textBottomPercent = percent
        this.bottomFontSize = this.finalFontSize * percent
    },
    handleTextTop: function(percent){
        this.textTopPercent = percent
        this.topFontSize = this.finalFontSize * percent
    },
    b64ToUint8Array: function(b64Image) {
       var img = atob(b64Image.split(',')[1]);
       var img_buffer = [];
       var i = 0;
       while (i < img.length) {
          img_buffer.push(img.charCodeAt(i));
          i++;
       }
       return new Uint8Array(img_buffer);
    },
    generateMeme: function(){
        html2canvas(document.querySelector("#meme-image-for-screenshot"), {width:500, height:500}).then(canvas => {
            // document.body.appendChild(canvas)

            var b64Image = canvas.toDataURL();
            var u8Image  = this.b64ToUint8Array(b64Image);
            var blob = new Blob([ u8Image ], {type: "image/png", name:'file.png'});

            var formData = new FormData();
            formData.append("image", blob);

            axios.post("save-meme", formData)
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });

        });
    },
      handleRemove: function(file, fileList) {
        console.log(file, fileList);
      },
      handleChange: function(file) {

        var fr = new FileReader;

        fr.onload = function() { // file is loaded
            var img = new Image;

            img.onload = function() {
                // console.log(img.width, img.height); // image is loaded; sizes are available
                var percentPreview = 1
                var percentFinal = 1

                if(img.width > this.previewMemeWidth || img.height > this.previewMemeHeight){

                    if(img.width > img.height){
                        percentPreview = this.previewMemeWidth / img.width
                        percentFinal = this.finalMemeWidth / img.width
                    } else {
                        percentPreview = this.previewMemeHeight / img.height
                        percentFinal = this.finalMemeHeight / img.height
                    }
                }

                this.previewImageWidth = img.width * percentPreview
                this.previewImageHeight = img.height * percentPreview

                this.finalImageWidth = img.width * percentFinal
                this.finalImageHeight = img.height * percentFinal

                
                this.imageUrl = URL.createObjectURL(file.raw);
            }.bind(this);

            img.src = fr.result; // is the data URL because called with readAsDataURL
        }.bind(this);

        fr.readAsDataURL(file.raw);
      }
    }
})