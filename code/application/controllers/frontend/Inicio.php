<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . "/controllers/frontend/FrontendBaseController.php";

class Inicio extends FrontendBaseController {
  public function __construct()
  {
      parent::__construct();
  }

  public function index() {

  }

  public function memeGenerator()
  {

    $this->minify
      ->js(['meme-generator.js'], "extra");
    
    $this->render('frontend/meme-generator', [
      "test" => 1,
    ]);
  }

  public function saveMeme()
  {
    print_r($_FILES);

    $config['upload_path']          = './assets/uploads/';
    $config['allowed_types']        = '*';
    $config['max_size']             = 1143540;
    $config['file_name']            = "meme-".uniqid().".png";

    $this->load->library('upload', $config);

    if ( ! $this->upload->do_upload('image'))
    {
            // $error = array('error' => $this->upload->display_errors());

            // $this->load->view('upload_form', $error);
      echo "error";
      echo  $this->upload->display_errors();
    }
    else
    {
            // $data = array('upload_data' => $this->upload->data());
      echo "success";
            // $this->load->view('upload_success', $data);
    }

  }
}
