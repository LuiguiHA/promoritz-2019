<?php
$lang['error_usuario_datos_registro'] = 'Datos de usuario inválidos, verifique que no sean nulos';
$lang['error_usuario_existe_correo'] = 'Ya existe un usuario con ese correo.';
$lang['error_usuario_existe_dni'] = 'Ya existe un usuario con ese DNI.';
$lang['error_usuario_registrado_dispositivo'] = 'Ya existe un usuario con esos datos, pero no se encontró datos del dispositivo.';
$lang['error_dni_invalido'] = 'El DNI es inválido.';
$lang['error_sexo_invalido'] = 'El sexo es inválido.';
$lang['error_celular_invalido'] = 'El celular es inválido.';
$lang['error_correo_invalido'] = 'El correo es inválido.';
$lang['error_persona_registro'] = 'No se pudo registrar a la persona.';
$lang['error_usuario_registro'] = 'No se pudo registrar al usuario.';
$lang['error_dispositivo_registro'] = 'No se pudo registrar el dispositivo.';
$lang['error_usuario_dispositivo_registro'] = 'No se pudo registrar usuario dispositivo.';
$lang['error_aplicacion'] = 'Aplicacion no válida.';

$lang['error_usuario_registrado'] = 'El usuario no se encuentra registrado.';
$lang['error_usuario_actualizar_sesion'] = 'No se pudo actualizar la sesión del usuario.';
$lang['error_credencial'] = 'Credenciales no válidas.';
$lang['error_usuario_contrasenia'] = 'Contraseña incorrecta.';
$lang['error_cerrar_sesion'] = 'No se pudo cerrar sesión.';
$lang['error_token_envio'] = 'Token no recibido.';
$lang['error_token'] = 'Usuario no autorizado. Token inválido.';
$lang['error_usuario_valido'] = 'Usuario no válido.';
$lang['error_usuario_actualizar'] = 'No se pudo actualizar el usuario.';
$lang['error_obtener_token_push'] = 'No se pudo obtener el token push.';
$lang['error_actualizar_token_push'] = 'No se pudo actualizar el token push.';



$lang['error_email_vacio'] = 'El email no puede ser vacio.';
$lang['error_email_invalido'] = 'Email inválido.';
$lang['error_campos_vacios'] = 'Los siguientes campos estan vacios: %s .';
$lang['error_password_vacio'] = 'El password no puede ser vacio.';
$lang['error_usuario_no_encontrado'] = 'Credenciales inválidas, por favor verifica que tu email este correcto o que tu contraseña sea la correcta.';
$lang['error_servidor_problemas'] = 'El servidor presentó un problema, por favor vuelve a intentarlo en unos minutos.';
$lang['error_usuario_no_autorizado'] = 'No tienes autorización para obtener la información solicitada.';
$lang['error_recurso_no_encontrado'] = 'La información solicitada no se encuentra disponible.';
$lang['error_usuario_dispositivo_inválido'] = 'Usuario dispositivo inválido.';
$lang['error_dispositivo_actualizar'] = 'No se pudo actualizar el dispositivo.';
$lang['error_usuario_inicio_sesion'] = 'No se pudo iniciar sesion.';
$lang['error_usuario_red_social'] = 'Inicio de Sesión con la red social incorrecta.';
$lang['error_usuario_perfil'] = 'No se pudo obtener el perfil de usuario';

$lang['error_promocion_categoria'] = 'No se pudo obtener las categorías de las promociones.';
$lang['error_promocion_listar'] = 'No se pudo obtener las promociones.';
$lang['error_categoria_parametro_valido'] = 'Ingresar una categoría válida.';
$lang['error_categoria_parametro'] = 'Debe ingresar el parámetro categoría.';
$lang['error_promocion_parametro'] =  'Debe ingresar el parámetro id de promoción.';
$lang['error_promocion_parametro_valido'] =  'Ingresar el id de promoción válida.';
$lang['error_promocion_obtener_detalle'] = 'No se pudo obtener el detalle de la promoción.';
$lang['error_promocion_obtener_informacion_adicional'] = 'No se pudo obtener la información adicional.';
$lang['error_promocion_obtener_foto'] = 'No se pudo obtener la foto de la promocion.';
$lang['error_promocion_obtener_empresa'] = 'No se pudo obtener el detalle de la empresa.';
$lang['error_promocion_obtener_local'] = 'No se pudo obtener los locales de la promoción.';
$lang['error_promocion_listar_vacio'] = 'No se encontro la promoción.';

$lang['error_beneficio_categoria'] = 'No se pudo obtener las categorías de los beneficios.';
$lang['error_beneficio_listar'] = 'No se pudo obtener los beneficios.';
$lang['error_beneficio_listar_vacio'] = 'No hay canjes disponibles.';
$lang['error_beneficio_buscar'] = 'No se pudo obtener la busqueda de beneficios.';
$lang['error_beneficio_categoria_parametro_valido'] = 'Ingresar una categoría válida.';
$lang['error_beneficio_categoria_parametro'] = 'Debe ingresar el parámetro categoría.';
$lang['error_beneficio_parametro'] = 'Debe ingresar el parámetro id del beneficio.';
$lang['error_beneficio_parametro_valido'] = 'Ingresar el id de beneficio válido.';
$lang['error_beneficio_detalle'] = 'No se pudo obtener el detalle del beneficio.';
$lang['error_beneficio_detalle_canje'] = 'No se pudo obtener el detalle del canje.';
$lang['error_beneficio_detalle_contacto'] = 'No se pudo obtener el contacto.';
$lang['error_beneficio_detalle_informacion_adicional'] = 'No se pudo obtener la información adicional.';
$lang['error_beneficio_detalle_foto'] = 'No se pudo obtener la foto del beneficio.';
$lang['error_beneficio_detalle_empresa'] = 'No se pudo obtener el detalle de la empresa.';
$lang['error_beneficio_detalle_locales'] = 'No se pudo obtener el detalle de los locales.';

$lang['error_puntos_listar'] = 'No se pudo obtener los puntos';
$lang['error_puntos_parametro_usuario'] = 'No se ingresó el parámetro de usuario';

$lang['error_canje_obtener_listar'] = 'No se pudo obtener los canjes';
$lang['error_canje_'] = 'No se pudo obtener los canjes';

$lang['error_carta_listar'] = 'No se pudo obtener el listado de cartas';
$lang['error_carta_listar_vacio'] = 'No se encontraron cartas disponibles.';
$lang['error_carta_listar_categoria'] = 'No se pudo listar las categoria de cartas';
$lang['error_carta_listar_categoria_parametro'] ='Se debe ingresar el id de la categoría.';
$lang['error_carta_detalle'] = 'No se pudo obtener el detalle de la carta';
$lang['error_carta_informacion_adicional'] = 'No se pudo obtener la informacion adicional de la carta';
$lang['error_carta_detalle_foto'] = 'No se pudo obtener el detalle de la foto';
$lang['error_carta_detalle_empresa'] = 'No se pudo obtener el detalle de la empresa';
$lang['error_carta_detalle_local'] = 'No se pudo obtener el detalle de los locales';
$lang['error_local_listado'] = 'No se pudo obtener el listado de locales';
$lang['error_horario_local_listado'] = 'No hay horarios disponibles para esta fecha';
$lang['error_horario_local'] = 'Debe ingresar el parámetro del local';
$lang['error_horario_fecha'] = 'Debe ingresar el parámetro de fecha';
$lang['error_ubicacion_listado'] = 'No se pudo obtener el listado de las ubicaciones de los locales';
$lang['error_solicitud_reserva_usuario'] = 'Debe ingresar parámetro usuario';
$lang['error_solicitud_reserva_local'] = 'Debe ingresar parámetro local';
$lang['error_solicitud_reserva_fecha'] = 'Debe ingresar parámetro fecha';
$lang['error_solicitud_reserva_hora'] = 'Debe ingresar parámetro hora';
$lang['error_solicitud_reserva_cantidad'] = 'Debe ingresar parámetro cantidad';
$lang['error_solicitud_reserva_ubicacion'] = 'Debe ingresar parámetro ubicación';
$lang['error_solicitud_reserva_horario'] = 'Debe ingresar parámetro horario';
$lang['error_solicitud_reserva_registro'] = 'No se puedo registrar la solicitud';

$lang['error_formato_fecha'] = 'El formato de fecha es incorrecto';

$lang['error_obtener_puntos_usuario'] = 'No se pudo obtener los puntos del usuario';


$lang['error_mensaje_usuario_dispositivo'] = 'Usuario no encontrado';
$lang['error_mensaje_estado_actualizado'] = 'No se pudo actualizar el mensaje';
$lang['error_mensaje_estado_parametros'] = 'No se envio los parametros obligatorios';

$lang['error_local_paquetes'] = 'No se pudo obtener los datos de paquetes del local';


