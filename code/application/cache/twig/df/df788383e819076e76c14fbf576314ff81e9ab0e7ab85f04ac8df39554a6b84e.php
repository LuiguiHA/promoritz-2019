<?php

/* frontend/layout/base.twig */
class __TwigTemplate_039185746d2ec471d76f3b61ca38c863dbf44fc8056e7e06e80e9e9d33d3429b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'extra_js' => [$this, 'block_extra_js'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!--PAGE: HOME -->
<!doctype html>
<html lang=\"es\">
<head>
<meta charset=\"utf-8\">
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
<title>Promo Ritz</title>
<link href=\"<?php echo base_url() ?>assets/css/favicon.ico\" rel=\"shortcut icon\" type=\"image/x-icon\" />
<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.6.1/css/all.css\" >
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, max-scale=1.0\">

<meta name=\"robots\" content=\"index, follow\" />
<!-- <meta content=\"Conoce nuestra línea de escritura fina para encontrar el regalo perfecto para esa persona tan especial. Sólo cuéntanos un poco sobre lo que buscas y te damos la mejor recomendación\" name=\"description\" />


<meta property=\"og:title\" content=\"Faber-Castell - Su Regalo Perfecto\" />
<meta property=\"og:description\" content=\"Conoce nuestra línea de escritura fina para encontrar el regalo perfecto para esa persona tan especial. Sólo cuéntanos un poco sobre lo que buscas y te damos la mejor recomendación\" />
<meta property=\"og:url\" content=\"http://www.suregaloperfecto.com\" />
<meta property=\"og:image\" content=\"<?php echo base_url() ?>assets/images/fabercastell_facebook.jpg\" /> -->

<link rel=\"stylesheet\" href=\"https://unpkg.com/element-ui@2.4.6/lib/theme-chalk/index.css\">
";
        // line 22
        echo (isset($context["css_minified"]) ? $context["css_minified"] : null);
        echo "

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src=\"js/html5shiv.min.js\"></script>
<![endif]-->


<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-112403330-1\"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-112403330-1');
</script> -->

</head>
<body>

";
        // line 44
        $this->displayBlock('content', $context, $blocks);
        // line 46
        echo "

<script>
</script>

<script src=\"https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.4/vue.min.js\"></script>
<script src=\"https://unpkg.com/element-ui@2.5.1/lib/index.js\"></script>

";
        // line 54
        $this->displayBlock('extra_js', $context, $blocks);
        // line 56
        echo "
";
        // line 57
        echo (isset($context["js_minified"]) ? $context["js_minified"] : null);
        echo "


</body>

<!--FOOTER -->
<footer class=\"footer\">
</footer>";
    }

    // line 44
    public function block_content($context, array $blocks = [])
    {
    }

    // line 54
    public function block_extra_js($context, array $blocks = [])
    {
    }

    public function getTemplateName()
    {
        return "frontend/layout/base.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 54,  98 => 44,  86 => 57,  83 => 56,  81 => 54,  71 => 46,  69 => 44,  44 => 22,  21 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!--PAGE: HOME -->
<!doctype html>
<html lang=\"es\">
<head>
<meta charset=\"utf-8\">
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
<title>Promo Ritz</title>
<link href=\"<?php echo base_url() ?>assets/css/favicon.ico\" rel=\"shortcut icon\" type=\"image/x-icon\" />
<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.6.1/css/all.css\" >
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, max-scale=1.0\">

<meta name=\"robots\" content=\"index, follow\" />
<!-- <meta content=\"Conoce nuestra línea de escritura fina para encontrar el regalo perfecto para esa persona tan especial. Sólo cuéntanos un poco sobre lo que buscas y te damos la mejor recomendación\" name=\"description\" />


<meta property=\"og:title\" content=\"Faber-Castell - Su Regalo Perfecto\" />
<meta property=\"og:description\" content=\"Conoce nuestra línea de escritura fina para encontrar el regalo perfecto para esa persona tan especial. Sólo cuéntanos un poco sobre lo que buscas y te damos la mejor recomendación\" />
<meta property=\"og:url\" content=\"http://www.suregaloperfecto.com\" />
<meta property=\"og:image\" content=\"<?php echo base_url() ?>assets/images/fabercastell_facebook.jpg\" /> -->

<link rel=\"stylesheet\" href=\"https://unpkg.com/element-ui@2.4.6/lib/theme-chalk/index.css\">
{{ css_minified | raw }}

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src=\"js/html5shiv.min.js\"></script>
<![endif]-->


<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-112403330-1\"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-112403330-1');
</script> -->

</head>
<body>

{% block content %}
{% endblock %}


<script>
</script>

<script src=\"https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.4/vue.min.js\"></script>
<script src=\"https://unpkg.com/element-ui@2.5.1/lib/index.js\"></script>

{% block extra_js %}
{% endblock %}

{{ js_minified | raw }}


</body>

<!--FOOTER -->
<footer class=\"footer\">
</footer>", "frontend/layout/base.twig", "/application/application/views/frontend/layout/base.twig");
    }
}
