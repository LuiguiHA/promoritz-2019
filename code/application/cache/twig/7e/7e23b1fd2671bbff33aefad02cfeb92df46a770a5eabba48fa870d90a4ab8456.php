<?php

/* frontend/meme-generator.twig */
class __TwigTemplate_921be585284f17b1373e299cfe2191e320935c79622c29249b20208d2056b04c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("frontend/layout/base.twig", "frontend/meme-generator.twig", 1);
        $this->blocks = [
            'content' => [$this, 'block_content'],
            'extra_js' => [$this, 'block_extra_js'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "frontend/layout/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        // line 4
        echo "
<script type=\"text/x-template\" id=\"input-autofit\">
    <div>
        <input readonly id=\"calculated\" :style=\"{
            'width': width + 'px', 
            'font-size': calculatedFontSize + 'px',
            'font-family': fontFamily,
            'text-align': 'center',
            'border': 'none',
            'background': 'none',
            'color': fontColor
        }\" ref=\"fitInput\" :value=\"text\">
        <span :style=\"{
            'font-size': fontSize + 'px',
            'font-family': fontFamily,
            'opacity': 0,
            'pointer-events': 'none'
        }\" ref=\"realInput\">\${text}</span>
    </div>
</script>


<div id=\"meme-generator\">
    <div id=\"meme-container\">
        <el-upload
          id=\"meme-image-upload\"
          :on-change=\"handleChange\"
          :on-remove=\"handleRemove\"
          multiple>
          <div v-if=\"imageUrl\" id=\"meme-image\" :style=\"{
            'width': previewMemeWidth + 'px',
            'height': previewMemeHeight + 'px'
          }\">
            <img :src=\"imageUrl\" :width=\"previewImageWidth\" :height=\"previewImageHeight\">
          </div>
          <div v-else>
              <i class=\"el-icon-upload\"></i>
              <div class=\"el-upload__text\">Suelta tu archivo aquí o <em>haz clic para cargar</em></div>
              <div slot=\"tip\" class=\"el-upload__tip\">Solo archivos jpg/png con un tamaño menor de 500kb</div>
          </div>
        </el-upload>
        <input-autofit 
          @input=\"handleTextTop\" 
          id=\"meme-top-text\" 
          :font-size=\"memeFontSize\" 
          :width=\"previewMemeWidth\"
          :font-family=\"fontFamily\"
          :text=\"textTop\"
          :font-color=\"colorTop\"
        ></input-autofit>
        <input-autofit 
          @input=\"handleTextBottom\" 
          id=\"meme-bottom-text\" 
          :font-size=\"memeFontSize\" 
          :width=\"previewMemeWidth\"
          :font-family=\"fontFamily\"
          :text=\"textBottom\"
          :font-color=\"colorBottom\"
        ></input-autofit>
        
    </div>
    <div>
      <div style=\"float:left; width:100%\">
        <input v-model=\"textTop\">
        <el-color-picker v-model=\"colorTop\"></el-color-picker>
      </div>
      <div style=\"float:left; width:100%\">
        <input v-model=\"textBottom\">
        <el-color-picker v-model=\"colorBottom\"></el-color-picker>
      </div>
      <button @click=\"generateMeme\">genera meme</button>
    </div>

    <div id=\"meme-final\">
      <div id=\"meme-image-for-screenshot\" v-if=\"imageUrl\">
        <img v-if=\"imageUrl\" :src=\"imageUrl\" :width=\"finalImageWidth\" :height=\"finalImageHeight\">
        <input id=\"top-text\" :style=\"{
          'font-size': topFontSize + 'px',
          'width': finalImageWidth + 'px',
          'text-align': 'center',
          'font-family': fontFamily,
          'border': 'none',
          'background': 'none',
          'pointer-events': 'none',
          'color': colorTop
        }\" :value=\"textTop\">
        <input id=\"bottom-text\" :style=\"{
          'font-size': bottomFontSize + 'px',
          'width': finalImageWidth + 'px',
          'text-align': 'center',
          'font-family': fontFamily,
          'border': 'none',
          'background': 'none',
          'pointer-events': 'none',
          'color': colorBottom
        }\" :value=\"textBottom\">
      </div>
    </div>
</div>
";
    }

    // line 105
    public function block_extra_js($context, array $blocks = [])
    {
        // line 106
        echo "<script src=\"assets/js/html2canvas.min.js\"></script>
<script src=\"https://unpkg.com/axios@0.18.0/dist/axios.min.js\"></script>
";
    }

    public function getTemplateName()
    {
        return "frontend/meme-generator.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 106,  135 => 105,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"frontend/layout/base.twig\" %}

{% block content %}

<script type=\"text/x-template\" id=\"input-autofit\">
    <div>
        <input readonly id=\"calculated\" :style=\"{
            'width': width + 'px', 
            'font-size': calculatedFontSize + 'px',
            'font-family': fontFamily,
            'text-align': 'center',
            'border': 'none',
            'background': 'none',
            'color': fontColor
        }\" ref=\"fitInput\" :value=\"text\">
        <span :style=\"{
            'font-size': fontSize + 'px',
            'font-family': fontFamily,
            'opacity': 0,
            'pointer-events': 'none'
        }\" ref=\"realInput\">\${text}</span>
    </div>
</script>


<div id=\"meme-generator\">
    <div id=\"meme-container\">
        <el-upload
          id=\"meme-image-upload\"
          :on-change=\"handleChange\"
          :on-remove=\"handleRemove\"
          multiple>
          <div v-if=\"imageUrl\" id=\"meme-image\" :style=\"{
            'width': previewMemeWidth + 'px',
            'height': previewMemeHeight + 'px'
          }\">
            <img :src=\"imageUrl\" :width=\"previewImageWidth\" :height=\"previewImageHeight\">
          </div>
          <div v-else>
              <i class=\"el-icon-upload\"></i>
              <div class=\"el-upload__text\">Suelta tu archivo aquí o <em>haz clic para cargar</em></div>
              <div slot=\"tip\" class=\"el-upload__tip\">Solo archivos jpg/png con un tamaño menor de 500kb</div>
          </div>
        </el-upload>
        <input-autofit 
          @input=\"handleTextTop\" 
          id=\"meme-top-text\" 
          :font-size=\"memeFontSize\" 
          :width=\"previewMemeWidth\"
          :font-family=\"fontFamily\"
          :text=\"textTop\"
          :font-color=\"colorTop\"
        ></input-autofit>
        <input-autofit 
          @input=\"handleTextBottom\" 
          id=\"meme-bottom-text\" 
          :font-size=\"memeFontSize\" 
          :width=\"previewMemeWidth\"
          :font-family=\"fontFamily\"
          :text=\"textBottom\"
          :font-color=\"colorBottom\"
        ></input-autofit>
        
    </div>
    <div>
      <div style=\"float:left; width:100%\">
        <input v-model=\"textTop\">
        <el-color-picker v-model=\"colorTop\"></el-color-picker>
      </div>
      <div style=\"float:left; width:100%\">
        <input v-model=\"textBottom\">
        <el-color-picker v-model=\"colorBottom\"></el-color-picker>
      </div>
      <button @click=\"generateMeme\">genera meme</button>
    </div>

    <div id=\"meme-final\">
      <div id=\"meme-image-for-screenshot\" v-if=\"imageUrl\">
        <img v-if=\"imageUrl\" :src=\"imageUrl\" :width=\"finalImageWidth\" :height=\"finalImageHeight\">
        <input id=\"top-text\" :style=\"{
          'font-size': topFontSize + 'px',
          'width': finalImageWidth + 'px',
          'text-align': 'center',
          'font-family': fontFamily,
          'border': 'none',
          'background': 'none',
          'pointer-events': 'none',
          'color': colorTop
        }\" :value=\"textTop\">
        <input id=\"bottom-text\" :style=\"{
          'font-size': bottomFontSize + 'px',
          'width': finalImageWidth + 'px',
          'text-align': 'center',
          'font-family': fontFamily,
          'border': 'none',
          'background': 'none',
          'pointer-events': 'none',
          'color': colorBottom
        }\" :value=\"textBottom\">
      </div>
    </div>
</div>
{% endblock %}

{% block extra_js %}
<script src=\"assets/js/html2canvas.min.js\"></script>
<script src=\"https://unpkg.com/axios@0.18.0/dist/axios.min.js\"></script>
{% endblock %}
", "frontend/meme-generator.twig", "/application/application/views/frontend/meme-generator.twig");
    }
}
