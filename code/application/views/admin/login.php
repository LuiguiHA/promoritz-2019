
<body class="text-center">
  

<?php echo form_open("admin/login", array('class' => 'form-signin'));?>
  
<h1 class="h3 mb-3 font-weight-normal">Administrador</h1>

<div id="infoMessage"><?php echo $message;?></div>

  <p>
    <?php echo form_input($identity);?>
  </p>

  <p>
    <?php echo form_input($password);?>
  </p>


  <p><?php echo form_submit('submit', lang('login_submit_btn'), array("class" => "btn btn-lg btn-primary btn-block"));?></p>

<?php echo form_close();?>
</body>