<!--PAGE: HOME -->
<section class="pageHome" id="home">
	<h1>Faber-Castell - Su regalo perfecto</h1>
	<div class="intro" anim>
      <a class="ico02" href="#regalo-perfecto">Su Regalo Perfecto</a>
      <a class="ico01" href="#linea-escritura-fina">Línea de Escritura Fina</a>
      <a class="ico03" href="#contacto">Encuéntranos</a>
	</div>
</section>
<!--PAGE: REGALO -->
<section class="pageRegalo" id="regalo-perfecto">
	
	<div class="content" anim>
		<h2>Encuentre el regalo perfecto</h2>
		<!--<p>¡Personaliza tu bolígrafo y sorprende!. Productos personalizados para sorprender a clientes, empleados o para utilizar como material promocional. Envíanos tu logo en formato PNG (fondo transparente), elige el instrumento de escritura deseado y te mostraremos cómo queda el producto terminado.</p>-->
		<p>¡Anímate a encontrar el regalo perfecto para esa persona especial!</p>
		<!--FORM -->
		<div class="formBuscar">
			<form id="form1" name="form1" method="post">
				<!--COLUMN -->
				<div class="col">
					<select name="cboSexo" id="cboSexo" required>
						<option value="0">Sexo</option>
						<option value="F">Femenino</option>
						<option value="M">Másculino</option>
					</select>
					<select name="cboEdad" id="cboEdad" required>
						<option value="0">Edad</option>
						<?php 
                          if (isset($edades) && count($edades) > 0) {
                            foreach ($edades as $edad) {
                              echo '<option value="'.$edad->id.'">'.$edad->descripcion.'</option>';
                            }
                          }
                        ?>
					</select>
					<select name="cboTipo" id="cboTipo" required>
						<option value="0">Tipo</option>
						<?php 
                          if (isset($tipos) && count($tipos) > 0) {
                            foreach ($tipos as $tipo) {
                              echo '<option value="'.$tipo->id.'">'.$tipo->descripcion.'</option>';
                            }
                          }
                        ?>
					</select>
				</div>
				<!--COLUMN -->
				<div class="col">
					<select name="cboPersonalidad" id="cboPersonalidad" required>
						<option value="0">Estilo / Personalidad</option>
						<?php 
                          if (isset($personalidades) && count($personalidades) > 0) {
                            foreach ($personalidades as $personalidad) {
                              echo '<option value="'.$personalidad->id.'">'.$personalidad->descripcion.'</option>';
                            }
                          }
                        ?>
					</select>
					<select name="cboTono" id="cboTono" required>
						<option value="0">Gama de colores</option>
						<?php 
                          if (isset($tonos) && count($tonos) > 0) {
                            foreach ($tonos as $tono) {
                              echo '<option value="'.$tono->id.'" data-imagesrc="'.base_url().'assets/images/tonos/'.$tono->alias.'.png">'.$tono->descripcion.'</option>';
                            }
                          }
                        ?>
					</select>
					<input class="btn" type="submit" name="submit" value="Buscar el regalo">
				</div>
			</form>
		</div>
	</div>
	
</section>


<!--PAGE: LINEA -->
<section class="pageLinea" id="linea-escritura-fina">
	
	<div class="content">
		<h2>Línea de escritura fina</h2>
		<p>Iniciar una nueva etapa de vida, redactar un proyecto y cambiar el mundo o añadir a alguien nuevo a tu mundo son momentos únicos y para aquellos momentos está la línea de escritura fina de Faber-Castell “Design”, una línea de productos clásica, elegante y purista con diseños que encuentran el balance perfecto entre la tradición y la innovación.</p>
		
		<!--CATALOG -->
		<div class="catalog">
			
			<!--FILTER MOVIL -->
			<select id="select" class="movFilter" name="select">
				<option value="0">Todos</option>
				<?php 
                  if (isset($categorias) && count($categorias) > 0) {
                    foreach ($categorias as $categoria) {
                      echo '<option value="'.$categoria->id.'">'.$categoria->nombre.'</option>';
                    }
                  }
                ?>
				
			</select>
			
			<!--FILTER -->
			<div class="filter">
				<ul>
					<li><a class="active" href="0">Todos</a></li>
					<?php 
                      if (isset($categorias) && count($categorias) > 0) {
                        foreach ($categorias as $categoria) {
                          echo '<li><a href="'.$categoria->id.'">'.$categoria->nombre.'</a></li>';
                        }
                      }
                    ?>
				</ul>
			</div>
			
			<!--PRODUCTS -->
			<div class="listProducts">
			
			  
				<!--GROUP -->
				<div class="slider">
                 
                  
                <?php
                  if (isset($productos) && count($productos) > 0) {

                    $separador = 0;
                    
                    echo '<div class="group">';
                    
                    foreach ($productos as $producto) {
                      //if($separador == 0) echo '<div class="group">';
                      
                      echo '<div class="item">
                                <a href="'.$producto->id.'" class="btnImg"><img src="'.base_url().'assets/images/catalogo/default.jpg" alt="'.$producto->nombre.'" data="'.$producto->sku.'"></a>
                                <h3>'.$producto->nombre.'</h3>
                                <p>SKU: '.$producto->sku.'</p>
                                <a class="btn" href="'.$producto->id.'">Ver más</a>
                            </div>';
                      $separador++;

                      /*if($separador == 8 || $total_productos == $producto->Id){ 
                        echo '</div>'; 
                        $separador = 0; 
                      }*/
                      if($separador == 6){ 
                        echo '</div><div class="group">'; 
                        $separador = 0; 
                      }
                    }
                    
                    echo '</div>'; 

                  }


                ?>
				</div>
				
			</div>
			
		</div>
		<!--PAGINATOR	-->
        <div class="pag">
            <span class="aPrev"></span>
            <span class="aNext active"></span>
        </div>
	</div>
	
</section>

<!--PAGE: CONTACTO -->
<section class="pageContacto" id="contacto">
	
	<div class="content">
		<!--MODULE: CONTACTO -->
		<div class="modContacto" anim>
			<h2>Contacto</h2>
			<p>Si tienes alguna consulta o si estás interesado en ventas corporativas de la línea de escritura fina, déjanos tu mensaje y a la brevedad un representante se comunicará contigo. ¡Muchas gracias!</p>
			<!--FORM -->
			<form id="form2" name="form2" method="post">
				<input class="itCampo" type="text" name="userNames" id="userNames" placeholder="Nombre y Apellido">
				<input class="itCampo" type="email" name="userEmail" id="userEmail" placeholder="E-mail">
				<textarea class="itArea" name="userMessage" id="userMessage" placeholder="Mensaje"></textarea>
				<label for="userData"><input type="checkbox" name="userData" id="userData">Acepto <a href="#" class="terminos">términos y condiciones</a>.</label>
				<label for="userData2"><input type="checkbox" name="userData2" id="userData2">Acepto recibir más información.</label>
				
				
				<!--<div class="re_captcha">
                  <div id="captcha_container"></div>
                </div>-->
                
                <div class="inError">
                  <p></p>
                </div>
                
                <input class="btn" type="submit" name="submit2" id="submit2" value="Enviar">
			</form>
			
		</div>
		<!--MODULE: PUNTOS -->
		<div class="modPuntos" anim>
			<h2>Puntos de venta</h2>
			<p>¡Encuentra el más cercano! <br>Tenemos un Faber-Castell cerca a ti. Encuentra nuestras tiendas aquí.</p>
			<div class="map">
				<iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBG7NQYnacAuqE0ZxELdXAdVBSq1LJ2Vvk&q=Faber-Castell Perú" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<a class="btn" href="#">Ver mapas</a>
		</div>
	</div>
	
</section>