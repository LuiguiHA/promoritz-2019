
<!-- PAGE CONTENT WRAPPER -->

<div class="page-content-wrap">

    <div class="container-fluid">
        <div class="col-md-12 panel-body">
            <div class="block" style="background-color:#EEEEEE;margin-bottom: 0px;padding-bottom:20px;">
                <form action="<?php echo site_url($this->config->item('path_backend').'/Carta/agregar');?>" id="agregar_carta" class="form-horizontal" role="form" name="agregar_carta" method="post"
                      enctype="multipart/form-data" >

                    <div class="form-group">
                        <label class="col-md-1 control-label">Categoría</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="categoria" id="categoria" >
                                <option value="">Seleccione</option>
                                <?php foreach ($categorias as $categoria) { ?>
                                    <option value="<?php echo $categoria->id;?>">
                                        <?php echo $categoria->nombre; ?>
                                    </option>
                                <?php } ?>

                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Local</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="local" id="local" >
                                <option value="">Seleccione</option>
                                <?php foreach ($local as $local) { ?>
                                    <option value="<?php echo $local->id;?>">
                                        <?php echo $local->nombre; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Nombre</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="nombre"  id="nombre" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Precio</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="precio"  id="precio" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Descripción</label>
                        <div class="col-md-11">

                            <textarea type="text" class="form-control" value="" name="descripcion" id="descripcion"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Estado</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="estado" id="estado" >
                                <option value="">Seleccione</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div>



                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-primary"  style="margin-right: .5em;" href="<?php echo site_url($this->config->item('path_backend').'/Carta');?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar" />
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">



   var jvalidate = $("#agregar_carta").validate({
       ignore: [],
       rules: {
           categoria: {
               required: true
           },

           nombre: {
               required: true
           },

           precio: {
               required: true

           },

           estado: {
               required: true
           }
       }
   });
   $(document).ready(function(){

       $('#precio').keypress(validateNumberDouble);
        
   });



   function validateNumberDouble(event) {
       var code = (event.which) ? event.which : event.keyCode;
       if(code==8 || code ==45 || code == 46)
       {
           //backspace
           return true;
       }
       else if(code>=48 && code<=57)
       {
           //is a number
           return true;
       }
       else
       {
           return false;
       }
   }




</script>
