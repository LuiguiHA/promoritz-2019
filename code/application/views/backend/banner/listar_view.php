<!-- PAGE CONTENT WRAPPER -->



        
        <div class="panel-body" >
            <table class="table table-hover table-cms" >
                <thead>
                <tr>
                    <td colspan="6" align="center" valign="top"></td>
                </tr>
                <tr>
                    <th>Recurso</th>
                    <th>Nombre</th>
                    <th>descripcion</th>
                    <th>Target</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>


                <?php
                     

                if (isset($banner) && count($banner) > 0) {

                    foreach ($banner as $banner) {
                        ?>
                        <tr>
                        
                            <td><?php echo $banner->recurso; ?></td>
                            <td><?php echo $banner->nombre; ?></td>
                            <td><?php echo $banner->descripcion; ?></td>
                            <td><?php echo $banner->target; ?></td>

                            <td>
                                <?php
                                if ($banner->estado == 1)
                                     echo '<span class="label label-success label-form" style="width:70px; font-weight:bolder;">Activo</span>';
                                else
                                     echo '<span class="label label-danger label-form" style="width:70px; font-weight:bolder;">Inactivo</span>';
                                ?>
                            </td>
                            <td>

                                <a href="<?php echo site_url($this->config->item('path_backend') . '/Banner/editar/'.$empresa.'/'.$banner->id); ?>"
                                   class="btn btn-info btn-condensed" title="Editar"><i class="fa fa-edit"></i></a>
                            </td>

                        </tr>
                        <?php
                    }

                }
                ?>

                </tbody>
            </table>
            <?php
            
            if (isset($paginador))
            echo $paginador;
            ?>
        </div>



<script>
    
       $(document).on("click", "#paginador ul li a", function(e){
           e.preventDefault();
           var href = $(this).attr("href");

          // alert(href);
           $.post(href, {id_empresa : <?php echo $empresa;?> , nombre :$("#nombre_banner").val() ,
                   tipo : $("#tipo_banner").val(),estado : $("#estado_banner").val()},
               function (result) {

                 $('#tabla_empresa_banner').html(result);


               }, "html");

           return true;
       });

     
</script>
<!-- END PAGE CONTENT WRAPPER -->