<!-- PAGE CONTENT WRAPPER -->

<div class="panel panel-default tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab1" data-toggle="tab">Local</a></li>
        <li><a href="#tab2" data-toggle="tab" id="lista_local_ubicacion">Ubicación</a></li>
        <li><a href="#tab3" data-toggle="tab" id="lista_local_horario">Horario</a></li>
    </ul>
    
    <div class="tab-content">
        <div class="tab-pane panel-body active" id="tab1">

            <form
                action="<?php echo site_url($this->config->item('path_backend') . '/Local_Empresa/editar/'.$empresa.'/'.$local_empresa->id); ?>"
                id="editar_local" class="form-horizontal" role="form" name="editar_local" method="post"
                enctype="multipart/form-data" >

                <div class="form-group">
                    <label class="col-md-1 control-label">Nombre</label>
                    <div class="col-md-11">
                        <input type="text" class="form-control" value="<?php echo $local_empresa->nombre;?>" name="nombre" id="nombre" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Dirección</label>
                    <div class="col-md-11">
                        <input type="text" class="form-control" value="<?php echo $local_empresa->direccion;?>" name="direccion" id="direccion" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Latitud</label>
                    <div class="col-md-11">
                        <input type="text" class="form-control" value="<?php echo $local_empresa->latitud;?>" name="latitud" id="latitud" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Longitud</label>
                    <div class="col-md-11">
                        <input type="text" class="form-control" value="<?php echo $local_empresa->longitud;?>" name="longitud" id="longitud" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Correo</label>
                    <div class="col-md-11">
                        <input type="text" class="form-control" value="<?php echo $local_empresa->correo;?>" name="correo" id="correo" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Telefono</label>
                    <div class="col-md-11">
                        <input type="text" class="form-control" value="<?php echo $local_empresa->telefono;?>" name="telefono" id="telefono" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Localidad</label>
                    <div class="col-md-11">
                        <input type="text" class="form-control" value="<?php echo $local_empresa->localidad;?>" name="localidad" id="localidad" />
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-1 control-label">Meses en Reserva</label>
                    <div class="col-md-11">
                        <input type="text" class="form-control spinner_default" value="<?php echo $local_empresa->meses_reserva;?>" id="meses" name="meses"  min="1"  readonly/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Estado</label>
                    <div class="col-md-11">
                        <select class="form-control select" name="estado" id="estado">
                            <option value="">Seleccione</option>
                            <option value="1"  <?php if($local_empresa->estado == "1") echo "selected";?>>Activo</option>
                            <option value="0" <?php if($local_empresa->estado == "0") echo "selected";?>>Inactivo</option>
                        </select>
                    </div>
                </div>


                <div class="btn-group pull-right">
                    <a type="button" class="btn btn-primary" style="margin-right: .5em;"
                       href="<?php echo site_url($this->config->item('path_backend').'/Empresa/editar/'.$empresa.'/L'); ?>">Cancelar</a>
                    <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar"/>
                </div>


            </form>
        </div>
        <div class="tab-pane panel-body " id="tab2">
            
            <div class="page-content-wrap">
                <div class="container-fluid">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="panel-body">
                                
                                <form name="form_busqueda_local_ubicacion" id="form_busqueda_local_ubicacion" method="post" action="">
                                    <div class="col-md-12 form-group">

                                        <div class="col-md-6">
                                            <label class="control-label"> Nombre</label>
                                            <input id="nombre_local_ubicacion" class="form-control input" type="text" name="nombre_local_ubicacion"
                                                   placeholder="Nombre" value="<?php echo $nombre; ?>"
                                                   class="input-small"/>
                                        </div>

                                        
                                        <div class="col-md-6">
                                            <label class="control-label">Estado</label>
                                            <select class="form-control select" name="estado_contacto" id="estado_local_ubicacion">
                                                <option value="">Seleccione</option>
                                                <option value="1" <?php if ($estado == "1") echo "selected"; ?>>Activo
                                                </option>
                                                <option value="0" <?php if ($estado == "0") echo "selected"; ?>>
                                                    Inactivo
                                                </option>
                                            </select>

                                        </div>
                                        <input type="hidden" name="buscar" id="buscar"
                                               value=""/>

                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-4">

                                        </div>
                                        <div class="col-md-8">
                                            <!--  <input type="submit" class="btn btn-primary pull-right" value="Buscar"
                                                    id="buscar_foto"/>-->
                                            <a href="#" class="btn btn-primary pull-right" id="buscar_local_ubicacion">Buscar</a>
                                        </div>

                                    </div>

                                </form>


                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Ubicaciones de local</h3>
                        <a  id="export_local_ubicacion" class="btn btn-danger dropdown-toggle pull-right"  style="margin-left: 5px;" ><img src="<?php echo base_url('assets/backend/');?>/img/icons/xls.png" width="16"/> Exportar</a>
                        <a href="<?php echo site_url($this->config->item('path_backend') . '/Local_ubicacion/agregar_view/' . $empresa .'/'. $local_empresa->id); ?>"
                           class="btn btn-default pull-right"><i class="fa fa-plus"></i> Agregar</a>

                    </div>
                    <div id="tabla_local_ubicacion"></div>

                </div>


            </div>
        </div>
        <div class="tab-pane panel-body " id="tab3">
            <div class="page-content-wrap">
                <div class="container-fluid">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="panel-body">

                                <form name="form_busqueda_local_horario" id="form_busqueda_local_horario" method="post" action="">
                                    <div class="col-md-12 form-group">


                                        <div class="col-md-6">
                                            <label class="control-label">Dia</label>
                                            <select class="form-control select" name="dia_local_horario" id="dia_local_horario">
                                                <option value="">Seleccione</option>
                                                <?php
                                                $day_week = unserialize (DAY_WEEK);
                                                    foreach ($day_week as $day) { ?>
                                                    <option value="<?php echo $day["id"];?>">
                                                        <?php echo $day["dia"]; ?>
                                                    </option>
                                                <?php } ?>
                                                
                                            </select>
                                        </div>

                                        <div class="col-md-6">
                                            <label class="control-label">Estado</label>
                                            <select class="form-control select" name="estado_local_horario" id="estado_local_horario">
                                                <option value="">Seleccione</option>
                                                <option value="1" <?php if ($estado == "1") echo "selected"; ?>>Activo
                                                </option>
                                                <option value="0" <?php if ($estado == "0") echo "selected"; ?>>
                                                    Inactivo
                                                </option>
                                            </select>
                                        </div>


                                        <input type="hidden" name="buscar" id="buscar"
                                               value=""/>

                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-4">

                                        </div>
                                        <div class="col-md-8">
                                            <!--  <input type="submit" class="btn btn-primary pull-right" value="Buscar"
                                                    id="buscar_foto"/>-->
                                            <a href="#" class="btn btn-primary pull-right" id="buscar_local_horario">Buscar</a>
                                        </div>

                                    </div>

                                </form>


                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Horario del local</h3>
                        <a  id="export_local_horario" class="btn btn-danger dropdown-toggle pull-right"  style="margin-left: 5px;" ><img src="<?php echo base_url('assets/backend/');?>/img/icons/xls.png" width="16"/> Exportar</a>
                        <a href="<?php echo site_url($this->config->item('path_backend') . '/Local_horario/agregar_view/' . $empresa .'/'. $local_empresa->id); ?>"
                           class="btn btn-default pull-right"><i class="fa fa-plus"></i> Agregar</a>

                    </div>
                    <div id="tabla_local_horario"></div>

                </div>


            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">

    $(function () {
        $(".spinner_default").spinner()
    });


    var jvalidate = $("#editar_local").validate({
        ignore: [],
        rules: {
            nombre: {
                required: true,
            },
            direccion: {
                required: true,
            },
            latitud: {
                required: true,
            },

            correo: {
                email: true,
                required: true
            },

            longitud: {
                required: true,
            },

            telefono: {
                required: true,
            },

            localidad: {
                required: true,
            },

            estado: {
                required: true
            }
        }
    });

    
    $("#lista_local_ubicacion").click(function () {

        $.post("<?php echo site_url('backend/Local_ubicacion/listar');?>", {
            id_local : <?php echo $local_empresa->id;?>,
            id_empresa : <?php echo $empresa;?> },
            function (result) {

                $('#tabla_local_ubicacion').html(result);


            }, "html");

        return true;
    });

    $("#buscar_local_ubicacion").click(function () {
        
        $.post("<?php echo site_url('backend/Local_ubicacion/buscar');?>",
            {   id_local : <?php echo $local_empresa->id;?> ,
                id_empresa : <?php echo $empresa;?>,
                nombre : $("#nombre_local_ubicacion").val() ,
                estado : $("#estado_local_ubicacion").val()},
            function (result) {
                $('#tabla_local_ubicacion').html(result);

            }, "html");

        return true;
    });


    $("#lista_local_horario").click(function () {

        $.post("<?php echo site_url('backend/Local_horario/listar');?>", {
                id_local : <?php echo $local_empresa->id;?>,
                id_empresa : <?php echo $empresa;?> },
            function (result) {

                $('#tabla_local_horario').html(result);


            }, "html");

        return true;
    });

    $("#buscar_local_horario").click(function () {

        $.post("<?php echo site_url('backend/Local_horario/buscar');?>",
            {   id_local : <?php echo $local_empresa->id;?> ,
                id_empresa : <?php echo $empresa;?>,
                dia : $("#dia_local_horario").val(),
                estado : $("#estado_local_horario").val()},
            function (result) {
                $('#tabla_local_horario').html(result);

            }, "html");

        return true;
    });

    $('#export_local_horario').click(function(){
        //  e.preventDefault();

        var url = "<?php echo site_url($this->config->item('path_backend').'/Local_Horario/export');?>";
        var id_empresa = <?php echo $empresa;?>;
        var id_local = <?php echo $local_empresa->id;?>;
        var estado = $("#estado_local_horario").val();
        var dia = $("#dia_local_horario").val();

        window.location.href= url+"?id_empresa="+id_empresa+"&id_local="+id_local+"&estado="+estado+"&dia="+dia;


    });

    $('#export_local_ubicacion').click(function(){
        //  e.preventDefault();

        var url = "<?php echo site_url($this->config->item('path_backend').'/Local_Ubicacion/export');?>";
        var id_empresa = <?php echo $empresa;?>;
        var id_local = <?php echo $local_empresa->id;?>;
        var nombre = $("#nombre_local_ubicacion").val();
        var estado = $("#estado_local_ubicacion").val();

        window.location.href= url+"?id_empresa="+id_empresa+"&id_local="+id_local+"&nombre="+nombre+"&estado="+estado;


    });


    <?php
    switch($this->uri->segment(6)){
    case 'U' :
    ?>
    $(document).ready(function () {
        $('#lista_local_ubicacion').trigger('click');
    });
    <?php  break;
    case 'H' :
    ?>
    $(document).ready(function () {
        $('#lista_local_horario').trigger('click');
    });
    <?php
    break;
    }
    ?>


</script>
