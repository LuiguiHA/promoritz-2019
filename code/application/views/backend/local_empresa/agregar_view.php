<!-- PAGE CONTENT WRAPPER -->

<div class="page-content-wrap">

    <div class="container-fluid">
        <div class="col-md-12 panel-body">
            <div class="block" style="background-color:#EEEEEE;margin-bottom: 0px;padding-bottom:20px;">
                <form
                    action="<?php echo site_url($this->config->item('path_backend') . '/Local_Empresa/agregar/'.$empresa); ?>"
                    id="agregar_local" class="form-horizontal" role="form" name="agregar_local" method="post"
                    enctype="multipart/form-data" >

                    <div class="form-group">
                        <label class="col-md-1 control-label">Nombre</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="nombre" id="nombre" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Dirección</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="direccion" id="direccion" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Latitud</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="latitud" id="latitud" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Longitud</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="longitud" id="longitud" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Correo</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="correo" id="correo" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Telefono</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="telefono" id="telefono" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Localidad</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="localidad" id="localidad" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Meses en Reserva</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control spinner_default" value="1" id="meses" name="meses"  min="1"  readonly/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Estado</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="estado" id="estado">
                                <option value="">Seleccione</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div>


                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-primary" style="margin-right: .5em;"
                           href="<?php echo site_url($this->config->item('path_backend').'/Empresa/editar/'.$empresa.'/L'); ?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar"/>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">

    $(function () {
        $(".spinner_default").spinner()
    });

    var jvalidate = $("#agregar_local").validate({
        ignore: [],
        rules: {
            nombre: {
                required: true,
            },
            direccion: {
                required: true,
            },
            latitud: {
                required: true,
            },

            correo: {
                email: true,
                required: true
            },
            cantidad: {
                required: true,
            },

            localidad: {
                required: true,
            },

            telefono: {
                required: true,
            },

            longitud: {
                required: true,
            },

            estado: {
                required: true
            }
        }
    });



</script>
