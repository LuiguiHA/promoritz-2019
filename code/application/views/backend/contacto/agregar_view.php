<!-- PAGE CONTENT WRAPPER -->

<div class="page-content-wrap">

    <div class="container-fluid">
        <div class="col-md-12 panel-body">
            <div class="block" style="background-color:#EEEEEE;margin-bottom: 0px;padding-bottom:20px;">
                <form
                    action="<?php echo site_url($this->config->item('path_backend') . '/Contacto/agregar/'.$empresa); ?>"
                    id="agregar_contacto" class="form-horizontal" role="form" name="agregar_contacto" method="post"
                    enctype="multipart/form-data" >

                    <div class="form-group">
                        <label class="col-md-1 control-label">DNI</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="dni" id="dni" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Nombre</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="nombre" id="nombre" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Apellido</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="apellido" id="apellido" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Sexo</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="sexo" id="sexo">
                                <option value="">Seleccione</option>
                                <option value="M">Masculino</option>
                                <option value="F">Femenino</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Fecha de Nacimiento</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="fecha_nacimiento" id="fecha_nacimiento"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Email</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" value="" name="email" id="email" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-1 control-label">Roles</label>
                        <div class="col-md-11">
                            <select class="form-control select" name="roles[]" id="roles"  multiple  required title="Seleccione">

                                <option value="dashboard">Dashboard</option>
                                <option value="clientes">Clientes</option>
                                <option value="reportes">Reportes</option>
                                <option value="consultas">Consultas</option>
                                    *
                            </select>



                            <div id="opciones-roles" style="margin-top: 1px;">

                                <div class="form-group hidden" id="rol-contacto">
                                    <label class="col-md-2 control-label">Consultas</label>
                                    <div class="col-md-10">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <div class="col-md-4">
                                                        <label class="check"><input type="checkbox" id="promociones" name="promociones"
                                                                                    class="icheckbox"/> Promociones</label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="check"><input type="checkbox" id="canje" name="canje"
                                                                                    class="icheckbox"/>
                                                            Canje</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>


                    <div class="form-group">
                        <label class="col-md-1 control-label">Estado</label>
                        <div class="col-md-11">
                            <select class="form-control " name="estado" id="estado">
                                <option value="">Seleccione</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div>


                    <div class="btn-group pull-right">
                        <a type="button" class="btn btn-primary" style="margin-right: .5em;"
                           href="<?php echo site_url($this->config->item('path_backend').'/Empresa/editar/'.$empresa.'/B'); ?>">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar">
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
<script type="text/javascript">



    var jvalidate = $("#agregar_contacto").validate({
        ignore: [],
        rules: {
            dni: {
                required: true,

            },
            nombre: {
                required: true,
            },
            apellido: {
                required: true,
            },
            sexo: {
                required: true,
            },
            email: {
                required: true,
                email:true,
            },
            rol: {
                required: true,

            },
            
            estado: {
                required: true
            }
        }


    });

    $(document).ready(function(){
        $('#dni').keypress(validateNumber);


    });

    function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;

        if (event.keyCode === 8 || event.keyCode === 46
            || event.keyCode === 37 || event.keyCode === 39) {
            return true;
        }
        else if ( key < 48 || key > 57 ) {
            return false;
        }
        else return true;
    }


    $("#agregar_contacto").change(function() {

        var rol = $('#roles').val();
       // var y = rol.split(',');

       // for (i = 0; i < y.length; i++) {

        if ( rol == "consultas") {
            document.getElementById("opciones-roles").setAttribute('style', 'visibility:visible');
            $("#rol-contacto").attr("class", "form-group");

        //}
    }

    });

    $("#fecha_nacimiento").datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'es'
    });

   
    
    /*
   $("#roles").change(function () {
       $("#rol-contacto").attr("class", "form-group hidden");


       var roles = $(this).val();
       if (roles != null) {
           for (var rol in roles) {
               if (rol == "3") {
                   $("#rol-contacto").attr("class", "form-group");
               }

           }
       }
   });
   jQuery.validator.messages.required = 'Por favor completar este campo.';

   $("#agregar_contacto").validate({
       highlight: function (element) {
           $(element).closest('.form-group').addClass('has-error');
       },
       unhighlight: function (element) {
           $(element).closest('.form-group').removeClass('has-error');
       },
       errorElement: 'span',
       errorClass: 'help-block',
       errorPlacement: function (error, element) {
           if (element.parent('.input-group').length) {
               error.insertAfter(element.parent());
           } else {
               error.insertAfter(element);
           }
       },
       submitHandler: function () {
           agregar();
       }
   });

   function agregar() {

       var promocheck = "no";
       var canjecheck = "no";
       var bd = "no";
       var client = "no";
       var report = "no";



       var url = $("#agregar_contacto").attr("action");
       var roles = $('#roles').val();

       if (roles != null) {
           for (var rol in roles) {
               if (rol == "3") {
                   if ($("#promociones").prop('checked')) {
                       promocheck = "si";
                   }
                   if ($("#canje").prop('checked')) {
                       canjecheck = "si";
                   }
               }
               if (rol = "0") {
                   bd = "si";
               }
               if (rol = "2") {
                   report = "si";
               }
               if (rol = "1") {
                   client = "si";
               }
              

           }
       }
       var rolJson = {
           "dashboard": {bd},
           "clientes": {client},
           "reportes": {report},
           "consultas": {"promocion": promocheck, "canje": canjecheck}

       };
       var validate = false;
       $.each(rolJson, function (k, v) {
           if (v.lectura == true || v.escritura == true) {
               validate = true;
           }
       });
       //if (validate) {
           var data = {
               "nombre": $("#nombre").val(),
               "apellido": $("#apellido").val(),
               "cargos": $("#cargos").val(),
               "usuario": $("#usuario").val(),
               "password": $("#password").val(),
               "rol": rolJson
              // "estado": $("#estado").val()
           };
           $.ajax({
               url: url,
               data: data,
               dataType: "json",
               type: "POST"
           }).done(function (responder) {
               if (responder.respuesta == 1) {
                   alert("Usuario registrado correctamente.");
                   location.href = "<?php echo site_url($this->config->item('path_backend') . '/usuarios');?>";
               }
               else {
                   console.log(responder);
                   alert("El usuario no pudo ser registrado correctamente");
               }
           });
       }
       else{
           alert("Por favor seleccionar almenos una opcion en roles");
       }
   }*/















</script>
