<!-- PAGE CONTENT WRAPPER -->

<div class="panel panel-default tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab1" data-toggle="tab">Beneficio</a></li>
        <li><a href="#tab2" data-toggle="tab" id="lista_beneficio_foto">Foto</a></li>
        <li><a href="#tab3" data-toggle="tab" id="lista_beneficio_info">Información Adicional</a></li>

        <?php if ($beneficio->tipo_locales != STATUS_DELIVERY){?>
        <li><a href="#tab4" data-toggle="tab" id="lista_beneficio_local">Locales validos</a></li>
        <?php }?>
    </ul>


    <div class="tab-content">
        <div class="tab-pane panel-body active" id="tab1">


            <form action="<?php echo site_url('/backend/Beneficio/editar/' . $beneficio->id) ?>" id="editar_beneficio"
                  class="form-horizontal" role="form" name="editar_beneficio" method="post"
                  enctype="multipart/form-data">

                <div class="form-group">
                    <label class="col-md-2 control-label">Categoría</label>
                    <div class="col-md-10">
                        <select class="form-control select" name="categoria" id="categoria" >
                            <option value="">Seleccione</option>
                            <?php foreach ($categorias as $categoria) { ?>
                                <option
                                    value="<?php echo $categoria->id; ?>" <?php if ($beneficio->id_beneficio_categoria == $categoria->id) {
                                    echo "selected";
                                } ?> >
                                    <?php echo $categoria->nombre; ?>
                                </option>
                            <?php } ?>

                        </select>
                    </div>
                </div>
                <?php

                ?>
                <div class="form-group">
                    <label class="col-md-2 control-label">Empresa</label>
                    <div class="col-md-10">
                        <select class="form-control select" name="empresa" id="empresa" <?php if ($empresas !=null && sizeof($empresas) == 1) echo "disabled"?>>
                            <option value="">Seleccione</option>
                            <?php foreach ($empresas as $empresa) { ?>
                                <option
                                    value="<?php echo $empresa->id; ?>" <?php if ($beneficio->id_empresa == $empresa->id) {
                                    echo "selected";
                                } ?>>
                                    <?php echo $empresa->nombre; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <?php if ($empresas != null && sizeof($empresas) == 1){
                ?>
                <input type="hidden" name="empresa" value="<?php  echo $empresas[0]->id?>" />
                <?php }
                ?>

                <div class="form-group">
                    <label class="col-md-2 control-label">Nombre</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" value="<?php echo $beneficio->nombre; ?>" name="nombre"
                               id="nombre"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Descripción</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" value="<?php echo $beneficio->descripcion; ?>"
                               name="descripcion" id="descripcion"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Restricción</label>
                    <div class="col-md-10">

                        <textarea type="text" class="form-control" name="restriccion"
                                  id="restriccion"><?php echo $beneficio->restriccion; ?></textarea>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-2 control-label">Tipo</label>
                    <div class="col-md-10">
                        <select class="form-control select" name="tipo" id="tipo">
                            <option value="">Seleccione</option>
                            <option value="1" <?php if ($beneficio->tipo == 1) {
                                echo "selected";
                            } ?> >Canje por puntos
                            </option>
                            <option value="2" <?php if ($beneficio->tipo == 2) {
                                echo "selected";
                            } ?>>Mixto
                            </option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Puntos</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" value="<?php echo $beneficio->puntos; ?>" name="puntos"
                               id="puntos"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Adicional</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" value="<?php echo $beneficio->monto_adicional_descripcion; ?>"
                               name="monto_adicional" id="monto_adicional"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Disponible en todos los locales </label>
                    <div class="col-md-10">
                        <select class="form-control select" name="tipo_locales" id="tipo_locales" required>
                            <option value="">Seleccione</option>
                            <option value="<?php echo STATUS_ALL_LOCALS?>" <?php if ($beneficio->tipo_locales == STATUS_ALL_LOCALS) echo "selected"?>>Si</option>
                            <option value="<?php echo STATUS_SOME_LOCALS?>" <?php if ($beneficio->tipo_locales == STATUS_SOME_LOCALS) echo "selected"?>>Algunos</option>
                            <option value="<?php echo STATUS_DELIVERY?>" <?php if ($beneficio->tipo_locales == STATUS_DELIVERY) echo "selected"?>>Delivery</option>
                        </select>
                    </div>
                </div>


                <div id="div_some_locals" style="margin-bottom: 15px " >
                    <div class="form-group">
                        <label class="col-md-2 control-label">Locales</label>
                        <div class="col-md-10">
                            <select  multiple class="form-control select" name="locales[]" id="locales">
                            <!--  aaa
                            <?php print_r($local_beneficio); ?>
                             -->
                                <?php if ($locales != null && sizeof($locales) > 0) { ?>
                                    <?php foreach ($locales as $local) { ?>
                                        <?php 
                                            $selected="";
                                            foreach($local_beneficio as $itemLocalBeneficio){
                                                if($local->id == $itemLocalBeneficio->id_local){
                                                    $selected=' selected="selected"';
                                                    break;
                                                }                                                    
                                            }
                                        ?>
                                        <option value="<?php echo $local->id; ?>" <?php echo $selected;?>><?php echo $local->nombre; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div id="div_delivery" style="margin-bottom: 30px " >
                    <div class="form-group">
                        <label class="col-md-2 control-label">Dirección</label>
                        <div class="col-md-10">

                            <input type="text" class="form-control" value="<?php echo $beneficio->direccion?>" name="direccion" id="direccion" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Telefono</label>
                        <div class="col-md-10">
                            <input type="number" class="form-control" value="<?php echo $beneficio->telefono?>" name="telefono" id="telefono"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-2 control-label">Correo</label>
                        <div class="col-md-10">
                            <input type="email" class="form-control" value="<?php echo $beneficio->correo?>" name="correo" id="correo"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-10">
                            <div id="map" style="height: 400px;width: 100%"></div>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Coordenadas  </label>
                        <div class="col-md-10">
                            <div style="margin-bottom: 10px">
                                <label class="control-label">Latitud :</label>
                                <input type="number" class="form-control"  name="latitud" id="latitud" value="<?php echo $beneficio->latitud?>" readonly/>
                            </div>
                            <div>
                                <label class="control-label">Longitud :</label>
                                <input type="number" class="form-control"  name="longitud" id="longitud" value="<?php echo $beneficio->longitud?>" readonly/>
                            </div>
                        </div>
                    </div>

                </div>


                <div class="form-group">
                    <label class="col-md-2 control-label">Orden</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" value="<?php echo $beneficio->orden; ?>" name="orden"
                               id="orden"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Fecha de inicio</label>
                    <div class="col-md-10">

                        <input type="text" class="form-control"
                               value="<?php echo date('Y-m-d', strtotime($beneficio->fecha_inicio)); ?>"
                               name="fecha_inicio" id="fecha_inicio"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Fecha de finalización</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control"
                               value="<?php echo date('Y-m-d', strtotime($beneficio->fecha_fin)); ?>" name="fecha_fin"
                               id="fecha_fin"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label" >Estado</label>
                    <div class="col-md-10">
                        <select class="form-control select" name="estado" id="estado">
                            <option value="">Seleccione</option>
                            <option value="1" <?php if ($beneficio->estado == 1) {
                                echo "selected";
                            } ?> >Activo
                            </option>
                            <option value="0" <?php if ($beneficio->estado == 0) {
                                echo "selected";
                            } ?>>Inactivo
                            </option>
                        </select>
                    </div>
                </div>


                <div class="btn-group pull-right">
                    <a type="button" class="btn btn-primary" style="margin-right: .5em;"
                       href="<?php echo site_url($this->config->item('path_backend') . '/inicio'); ?>">Cancelar</a>
                    <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar"/>
                </div>


            </form>

        </div>
        <div class="tab-pane panel-body " id="tab2">
            <div class="page-content-wrap">
                <div class="container-fluid">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="panel-body">


                                <form name="form_busqueda" id="form_busqueda" method="post" action="">
                                    <div class="col-md-12 form-group">

                                        <div class="col-md-4">
                                            <label class="control-label">Tipo</label>
                                            <select class="form-control select" name="tipo_foto" id="tipo_foto">
                                                <option value="">Seleccione</option>
                                                <option value="1" <?php if ($tipo == "1") echo "selected"; ?>>Principal
                                                </option>
                                                <option value="2" <?php if ($tipo == "2") echo "selected"; ?>>
                                                    Detalle
                                                </option>
                                            </select>

                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Plataforma</label>
                                            <select class="form-control select" name="plataforma_foto" id="plataforma_foto">
                                                <option value="">Seleccione</option>
                                                <option value="1" <?php if ($plataforma == "1") echo "selected"; ?>>Web
                                                </option>
                                                <option value="2" <?php if ($plataforma == "2") echo "selected"; ?>>
                                                    Móvil
                                                </option>
                                            </select>

                                        </div>


                                        <div class="col-md-4">
                                            <label class="control-label">Estado</label>
                                            <select class="form-control select" name="estado_foto" id="estado_foto">
                                                <option value="">Seleccione</option>
                                                <option value="1" <?php if ($estado == "1") echo "selected"; ?>>Activo
                                                </option>
                                                <option value="0" <?php if ($estado == "0") echo "selected"; ?>>
                                                    Inactivo
                                                </option>
                                            </select>

                                        </div>
                                        <input type="hidden" name="buscar" id="buscar"
                                               value=""/>

                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-4">

                                        </div>
                                        <div class="col-md-8">
                                            <!--  <input type="submit" class="btn btn-primary pull-right" value="Buscar"
                                                    id="buscar_foto"/>-->
                                            <a href="#" class="btn btn-primary pull-right" id="buscar_beneficio_foto">Buscar</a>
                                        </div>

                                    </div>

                                </form>


                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Fotos</h3>
                        <a   id="export_foto_beneficio" class="btn btn-danger dropdown-toggle pull-right"  style="margin-left: 5px;" ><img src="<?php echo base_url('assets/backend/');?>/img/icons/xls.png" width="16"/> Exportar</a>

                        <a href="<?php echo site_url($this->config->item('path_backend') . '/Foto_Beneficio/agregar/' . $beneficio->id); ?>"
                           class="btn btn-default pull-right"><i class="fa fa-plus"></i> Agregar</a>

                    </div>
                    <div id="tabla_beneficio_foto"></div>

                </div>


            </div>
        </div>
        <div class="tab-pane panel-body " id="tab3">
            <div class="page-content-wrap">
                <div class="container-fluid">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="panel-body">
                                <form name="form_busqueda_info" id="form_busqueda_info" method="post" action="">
                                    <div class="col-md-12 form-group">


                                        <div class="col-md-4">
                                            <label class="control-label"> Campo</label>
                                            <input id="campo_info" class="form-control input" type="text" name="campo_info"
                                                   placeholder="Campo" value="<?php echo $campo; ?>"
                                                   class="input-small"/>
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label"> Valor</label>
                                            <input id="valor_info" class="form-control input" type="text" name="valor_info"
                                                   placeholder="Valor" value="<?php echo $valor; ?>"
                                                   class="input-small"/>
                                        </div>



                                        <div class="col-md-4">
                                            <label class="control-label">Estado</label>
                                            <select class="form-control select" name="estado_info" id="estado_info">
                                                <option value="">Seleccione</option>
                                                <option value="1" <?php if ($estado == "1") echo "selected"; ?>>Activo
                                                </option>
                                                <option value="0" <?php if ($estado == "0") echo "selected"; ?>>
                                                    Inactivo
                                                </option>
                                            </select>

                                        </div>
                                        <input type="hidden" name="buscar" id="buscar"
                                               value=""/>

                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-4">

                                        </div>
                                        <div class="col-md-8">
                                            <a href="#" class="btn btn-primary pull-right" id="buscar_beneficio_info">Buscar</a>
                                        </div>

                                    </div>

                                </form>


                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Infomación Adicional</h3>
                        <a   id="export_info_beneficio" class="btn btn-danger dropdown-toggle pull-right"  style="margin-left: 5px;" ><img src="<?php echo base_url('assets/backend/');?>/img/icons/xls.png" width="16"/> Exportar</a>
                        <a href="<?php echo site_url($this->config->item('path_backend') . '/Info_Adicional_Beneficio/agregar/' . $beneficio->id); ?>"
                           class="btn btn-default pull-right"><i class="fa fa-plus"></i> Agregar</a>

                    </div>
                    <div id="tabla_beneficio_info"></div>

                </div>


            </div>
        </div>
        <div class="tab-pane panel-body " id="tab4">
            <div class="page-content-wrap">
                <div class="container-fluid">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="panel-body">
                                <form name="form_busqueda_local" id="form_busqueda_local" method="post" action="">
                                    <div class="col-md-12 form-group">

                                        <div class="col-md-6">
                                            <label class="control-label"> Local</label>
                                            <input id="local_nombre" class="form-control input" type="text" name="local_nombre"
                                                   placeholder="Local" value=""
                                                   class="input-small"/>
                                        </div>




                                        <div class="col-md-6">
                                            <label class="control-label">Estado</label>
                                            <select class="form-control select" name="estado_local" id="estado_local">
                                                <option value="">Seleccione</option>
                                                <option value="1" <?php if ($estado == "1") echo "selected"; ?>>Seleccionar
                                                </option>
                                                <option value="0" <?php if ($estado == "0") echo "selected"; ?>>Quitar
                                                </option>
                                            </select>

                                        </div>


                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-4">

                                        </div>
                                        <div class="col-md-8">

                                            <a href="#" class="btn btn-primary pull-right" id="buscar_beneficio_local">Buscar</a>
                                        </div>

                                    </div>

                                </form>


                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Locales validos</h3>
                        <a   id="export_local_beneficio" class="btn btn-danger dropdown-toggle pull-right"  style="margin-left: 5px;" ><img src="<?php echo base_url('assets/backend/');?>/img/icons/xls.png" width="16"/> Exportar</a>
                        <?php if ($beneficio->tipo_locales != STATUS_ALL_LOCALS){?>
                        <a href="<?php echo site_url($this->config->item('path_backend') . '/Local_Beneficio/agregar/' . $beneficio->id); ?>"
                           class="btn btn-default pull-right"><i class="fa fa-plus"></i> Seleccionar</a>
                        <?php }?>
                    </div>
                    <div id="tabla_beneficio_local"></div>

                </div>


            </div>
        </div>
    </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
    <script type="text/javascript">
        $(document).ready(initMap);

        var map;
        var lastMarkerLat="";
        var lastMarkerLng="";
        var markes = [];

        function initMap() {

            var mapOptions = {
                zoom: 15,
                center: {lat: -12.135817, lng: -77.0135569},
                zoomControl: true,
                scaleControl: true,
                streetViewControl: false,
                mapTypeId: 'satellite'
            };

            map = new google.maps.Map(document.getElementById('map'), mapOptions);



            google.maps.event.addListener(map, 'click', function(event) {

                if(markes.length > 0){
                    markes[0].setMap(null);
                    markes = [];
                }

                placeMarker(event.latLng);

            });

        }

        function placeMarker(location) {
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });

            $('#latitud').val(marker.getPosition().lat());
            $('#longitud').val(marker.getPosition().lng());

            markes.push(marker);
        }


        $(".spinner_default").spinner();
        $("#div_delivery").hide();
        $("#div_some_locals").hide();

        $("#tipo_locales").change(function () {
            var tipo_locales = $("#tipo_locales").val();
            tipo_locales_cambios(tipo_locales);
        });

        tipo_locales_cambios(<?php echo $beneficio->tipo_locales?>);

        function tipo_locales_cambios(tipo_locales){
            $("#div_delivery").hide();
            $("#div_some_locals").hide();
            if (tipo_locales == "<?php echo STATUS_DELIVERY?>"){
                $("#div_delivery").show();
                $("#telefono").attr("required", "true");
                $("#correo").attr("required", "true");
                //google.maps.event.trigger(map, 'resize')

            }else if(tipo_locales == "<?php echo STATUS_SOME_LOCALS?>"){
                $("#div_some_locals").show();
                $("#locales").attr("required", "true");
            }else{
                $("#telefono").removeAttr('required');
                $("#correo").removeAttr('required');
                $("#locales").removeAttr('required');
                $("#telefono").val("");
                $("#correo").val("");
                $("#direccion").val("");
                $("#longitud").val("");
                $("#latitud").val("");
            }
        }

        $("#locales").selectpicker({
            noneSelectedText:''
        });


        var jvalidate = $("#editar_beneficio").validate({
            ignore: [],
            rules: {
                categoria: {
                    required: true
                },
                empresa: {
                    required: true

                },
                nombre: {
                    required: true
                },

                tipo: {
                    required: true

                },
                puntos: {
                    required: true

                },

                fecha_inicio: {
                    required: true
                },
                fecha_fin: {
                    required: true
                },
                estado: {
                    required: true
                }
            }
        });

        $(document).ready(function () {
            $('#telefono').keypress(validateNumber);
            $('#puntos').keypress(validateNumber);
            $('#orden').keypress(validateNumber);
        });

        function validateNumber(event) {
            var key = window.event ? event.keyCode : event.which;

            if (event.keyCode === 8 || event.keyCode === 46
                || event.keyCode === 37 || event.keyCode === 39) {
                return true;
            }
            else if (key < 48 || key > 57) {
                return false;
            }
            else return true;
        }

        function validateNumberDouble(event) {
            var code = (event.which) ? event.which : event.keyCode;
            if (code == 8 || code == 45 || code == 46) {
                //backspace
                return true;
            }
            else if (code >= 48 && code <= 57) {
                //is a number
                return true;
            }
            else {
                return false;
            }
        }

        $("#lista_beneficio_foto").click(function () {

            $.post("<?php echo site_url('backend/Foto_beneficio/listar');?>",{id_beneficio : <?php echo $beneficio->id ;?>},
                function (result) {

                    $('#tabla_beneficio_foto').html(result);


                }, "html");

            return true;
        });

        $("#buscar_beneficio_foto").click(function () {


            $.post("<?php echo site_url('backend/Foto_beneficio/buscar');?>",
                {id_beneficio : <?php echo $beneficio->id;?> , estado : $("#estado_foto").val() , tipo : $("#tipo_foto").val(),plataforma : $("#plataforma_foto").val()},

                function (result) {
                    $('#tabla_beneficio_foto').html(result);

                }, "html");

            return true;
        });


        $("#lista_beneficio_info").click(function () {

            $.post("<?php echo site_url('backend/Info_adicional_beneficio/listar');?>",{id_beneficio : <?php echo $beneficio->id ;?>},
                function (result) {

                    $('#tabla_beneficio_info').html(result);


                }, "html");

            return true;
        });

        $("#buscar_beneficio_info").click(function () {


            $.post("<?php echo site_url('backend/Info_adicional_beneficio/buscar');?>",
                {id_beneficio : <?php echo $beneficio->id;?> , campo : $("#campo_info").val() , valor : $("#valor_info").val()
                    , estado : $("#estado_info").val()},
                function (result) {
                    $('#tabla_beneficio_info').html(result);

                }, "html");

            return true;
        });

        $("#lista_beneficio_local").click(function () {

            $.post("<?php echo site_url('backend/Local_beneficio/listar');?>",{id_beneficio : <?php echo $beneficio->id ;?>},
                function (result) {

                    $('#tabla_beneficio_local').html(result);


                }, "html");

            return true;
        });

        $("#buscar_beneficio_local").click(function () {


            $.post("<?php echo site_url('backend/Local_beneficio/buscar');?>",
                {id_beneficio : <?php echo $beneficio->id;?> , local : $("#local_nombre").val(), estado : $("#estado_local").val()},
                function (result) {
                    $('#tabla_beneficio_local').html(result);

                }, "html");

            return true;
        });





        <?php
        switch($this->uri->segment(5)){
        case 'F' :
        ?>
        $(document).ready(function () {
            $('#lista_beneficio_foto').trigger('click');
        });
        <?php  break;
        case 'I' :
        ?>
        $(document).ready(function () {
            $('#lista_beneficio_info').trigger('click');
        });
        <?php  break;
        case 'L' :
        ?>
        $(document).ready(function () {
            $('#lista_beneficio_local').trigger('click');
        });
        <?php
        break;
        }
        ?>

        $('#export_foto_beneficio').click(function(){
            // e.preventDefault();

            var url = "<?php echo site_url($this->config->item('path_backend').'/Foto_Beneficio/export');?>";
            var id_beneficio = <?php echo $beneficio->id;?>;
            var tipo=$("#tipo_foto").val();
            var estado=$("#estado_foto").val();
            var plataforma=$("#plataforma_foto").val();

            window.location.href= url+"?id_beneficio="+id_beneficio+"&tipo="+tipo+"&estado="+estado+"&plataforma="+plataforma;


        });

        $('#export_info_beneficio').click(function(){
            // e.preventDefault();

            var url = "<?php echo site_url($this->config->item('path_backend').'/Info_Adicional_Beneficio/export');?>";
            var id_beneficio = <?php echo $beneficio->id;?>;
            var campo=$("#campo_info").val();
            var valor=$("#valor_info").val();
            var estado=$("#estado_info").val();

            window.location.href= url+"?id_beneficio="+id_beneficio+"&campo="+campo+"&valor="+valor+"&estado="+estado;


        });

        $('#export_local_beneficio').click(function(){
            // e.preventDefault();

            var url = "<?php echo site_url($this->config->item('path_backend').'/Local_Beneficio/export');?>";
            var id_beneficio = <?php echo $beneficio->id;?>;
            var local=$("#local_nombre").val();
            var estado=$("#estado_local").val();

            window.location.href= url+"?id_beneficio="+id_beneficio+"&local="+local+"&estado="+estado;

        });

        $("#fecha_inicio").datetimepicker({
            format: 'YYYY-MM-DD',
            locale: 'es'
        });


        $("#fecha_fin").datetimepicker({
            format: 'YYYY-MM-DD',
            locale: 'es'
        });

        


    </script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASbnMbZgifMOqhzVF2RreWxOlzHlQeG1c&callback=initMap"
></script>
