<!-- PAGE CONTENT WRAPPER -->

<div class="panel panel-default tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab1" data-toggle="tab">Empresa</a></li>
        <li><a href="#tab2" data-toggle="tab" id="lista_empresa_contacto">Contacto</a></li>
        <li><a href="#tab3" data-toggle="tab" id="lista_empresa_banner">Banner</a></li>
        <li><a href="#tab4" data-toggle="tab" id="lista_empresa_local">Local</a></li>

    </ul>


    <div class="tab-content">
        <div class="tab-pane panel-body active" id="tab1">
            <form action="<?php echo site_url($this->config->item('path_backend').'/Empresa/editar/'.$empresa->id);?>" id="editar_empresa" class="form-horizontal" role="form" name="editar_empresa" method="post"
                  enctype="multipart/form-data" >


                <div class="form-group">
                    <label class="col-md-1 control-label">Nombre</label>
                    <div class="col-md-11">
                        <input type="text" class="form-control" value="<?php echo $empresa->nombre;?>" name="nombre"  id="nombre" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Codigo</label>
                    <div class="col-md-11">
                        <input type="text" class="form-control" value="<?php echo $empresa->codigo;?>" name="codigo"  id="codigo" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Descripción</label>
                    <div class="col-md-11">

                        <textarea type="text" class="form-control" value="" name="descripcion" id="descripcion"><?php echo $empresa->descripcion;?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Ruc</label>
                    <div class="col-md-11">
                        <input type="text" class="form-control" value="<?php echo $empresa->ruc;?>" name="ruc" id="ruc" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Razón Social</label>
                    <div class="col-md-11">
                        <input type="text" class="form-control" value="<?php echo $empresa->razon_social;?>" name="razon_social" id="razon_social" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label" >Logo</label>
                    <div class="col-md-11">
                        <?php if ($empresa->logo ==null || $empresa->logo == ""){
                            ?>

                            <div class="input-group date col-md-6" id="dp-2">
                                <span class="file-input-name"> No cuenta con logo </span>
                            </div>
                        <?php } else {?>
                            <div class="input-group date col-md-6" id="dp-2">
                                <input type="button" class="btn btn-primary" onclick="lightbox_open();" value="Ver Imagen" style=" left: -83.9063px; top: 5px; ";/>

                            </div>
                        <?php }?>
                        <div id="light" >
                            <a href="#" onclick="lightbox_close();"><img src="<?php if ($empresa->logo!=null)
                       echo base_url() .$empresa->logo;
                                ?> "width="100%" /></a>
                        </div>
                        <div id="fade" onClick="lightbox_close();"></div>
                        </br>
                        <div class="input-group date col-md-6" id="dp-2">
                            <input type="file" class="fileinput btn-success" name="logo" id="logo"
                                   title="Seleccione una Imagen" style="left: -83.9063px; top: 5px;" accept=".jpg,.png,.jpeg">
                            <span class="file-input-name "></span>
                        </div>

                    </div>

                </div>
                <input class="hidden" type="text" value="<?php echo $empresa->logo?>" name="logo_antiguo"/>

                <div class="form-group">
                    <label class="col-md-1 control-label">Formula</label>
                    <div class="col-md-11">
                        <select class="form-control select" name="formula" id="formula" >
                            <option value="">Seleccione</option>
                            <?php foreach ($formulas as $formula) { ?>
                                <option value="<?php echo $formula->id;?>" <?php if ($empresa->formula == $formula->id) {
                                    echo "selected";
                                } ?>>
                                    <?php echo $formula->nombre; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Sitio Web</label>
                    <div class="col-md-11">
                        <input type="text" class="form-control" value="<?php echo $empresa->sitio_web;?>" name="sitio_web" id="sitio_web" />
                    </div>
                </div>



                <div class="form-group">
                    <label class="col-md-1 control-label">Estado</label>
                    <div class="col-md-11">
                        <select class="form-control select" name="estado" id="estado" required>
                            <option value="">Seleccione</option>
                            <option value="1" <?php if($empresa->estado == "1") echo "selected";?>>Activo</option>
                            <option value="0" <?php if($empresa->estado == "0") echo "selected";?>>Inactivo</option>
                        </select>
                    </div>
                </div>

                <input type="hidden" name="id_for_punt_emp" id="id_for_punt_emp"  value="<?php echo $empresa->id_for;?>"/>

                <div class="btn-group pull-right">
                    <a type="button" class="btn btn-primary"  style="margin-right: .5em;" href="<?php echo site_url($this->config->item('path_backend').'/Empresa');?>">Cancelar</a>
                    <input type="submit" class="btn btn-primary" value="Guardar" name="agregar" id="agregar" />
                </div>

            </form>

        </div>
        <div class="tab-pane panel-body " id="tab2">
            <div class="page-content-wrap">
                <div class="container-fluid">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="panel-body">


                                <form name="form_busqueda_contacto" id="form_busqueda_contacto" method="post" action="">
                                    <div class="col-md-12 form-group">

                                        <div class="col-md-3">
                                            <label class="control-label"> Nombre</label>
                                            <input id="nombre_contacto" class="form-control input" type="text" name="nombre_contacto"
                                                   placeholder="Nombre" value="<?php echo $nombre; ?>"
                                                   class="input-small"/>
                                        </div>

                                        <div class="col-md-3">
                                            <label class="control-label"> Apellido</label>
                                            <input id="apellido_contacto" class="form-control input" type="text" name="apellido_contacto"
                                                   placeholder="Apellido" value="<?php echo $apellido; ?>"
                                                   class="input-small"/>
                                        </div>

                                        <div class="col-md-3">
                                            <label class="control-label"> DNI</label>
                                            <input id="dni_contacto" class="form-control input" type="text" name="dni_contacto"
                                                   placeholder="DNI" value="<?php echo $dni; ?>"
                                                   class="input-small"/>
                                        </div>

                                        <div class="col-md-3">
                                            <label class="control-label">Estado</label>
                                            <select class="form-control select" name="estado_contacto" id="estado_contacto">
                                                <option value="">Seleccione</option>
                                                <option value="1" <?php if ($estado == "1") echo "selected"; ?>>Activo
                                                </option>
                                                <option value="0" <?php if ($estado == "0") echo "selected"; ?>>
                                                    Inactivo
                                                </option>
                                            </select>

                                        </div>
                                        <input type="hidden" name="buscar" id="buscar"
                                               value=""/>

                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-4">

                                        </div>
                                        <div class="col-md-8">
                                            <!--  <input type="submit" class="btn btn-primary pull-right" value="Buscar"
                                                    id="buscar_foto"/>-->
                                            <a href="#" class="btn btn-primary pull-right" id="buscar_empresa_contacto">Buscar</a>
                                        </div>

                                    </div>

                                </form>


                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Contactos</h3>
                        <a   id="export_contacto" class="btn btn-danger dropdown-toggle pull-right"  style="margin-left: 5px;" ><img src="<?php echo base_url('assets/backend/');?>/img/icons/xls.png" width="16"/> Exportar</a>
                        <a href="<?php echo site_url($this->config->item('path_backend') . '/Contacto/agregar/' . $empresa->id); ?>"
                           class="btn btn-default pull-right"><i class="fa fa-plus"></i> Agregar</a>

                    </div>
                    <div id="tabla_empresa_contacto"></div>

                </div>


            </div>
        </div>
        <div class="tab-pane panel-body " id="tab3">
            <div class="page-content-wrap">
                <div class="container-fluid">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="panel-body">
                                <form name="form_busqueda_banner" id="form_busqueda_banner" method="post" action="">
                                    <div class="col-md-12 form-group">


                                        <div class="col-md-4">
                                            <label class="control-label"> Nombre</label>
                                            <input id="nombre_banner" class="form-control input" type="text" name="nombre_banner"
                                                   placeholder="Nombre" value="<?php echo $nombre; ?>"
                                                   class="input-small"/>
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Tipo</label>
                                            <select class="form-control select" name="tipo_banner" id="tipo_banner">
                                                <option value="">Seleccione</option>
                                                <option value="1" <?php if ($tipo == "1") echo "selected"; ?>>Principal
                                                </option>
                                                <option value="2" <?php if ($tipo == "2") echo "selected"; ?>>
                                                    Detalle
                                                </option>
                                            </select>

                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Estado</label>
                                            <select class="form-control select" name="estado_banner" id="estado_banner">
                                                <option value="">Seleccione</option>
                                                <option value="1" <?php if ($estado == "1") echo "selected"; ?>>Activo
                                                </option>
                                                <option value="0" <?php if ($estado == "0") echo "selected"; ?>>
                                                    Inactivo
                                                </option>
                                            </select>

                                        </div>
                                        <input type="hidden" name="buscar" id="buscar"
                                               value=""/>

                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-4">

                                        </div>
                                        <div class="col-md-8">
                                            <!--  <input type="submit" class="btn btn-primary pull-right" value="Buscar"
                                                    id="buscar_foto"/>-->
                                            <a href="#" class="btn btn-primary pull-right" id="buscar_empresa_banner">Buscar</a>
                                        </div>

                                    </div>

                                </form>


                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Banner</h3>
                        <a   id="export_banner" class="btn btn-danger dropdown-toggle pull-right"  style="margin-left: 5px;" ><img src="<?php echo base_url('assets/backend/');?>/img/icons/xls.png" width="16"/> Exportar</a>

                        <a href="<?php echo site_url($this->config->item('path_backend') . '/Banner/agregar/' . $empresa->id); ?>"
                           class="btn btn-default pull-right"><i class="fa fa-plus"></i> Agregar</a>

                    </div>
                    <div id="tabla_empresa_banner"></div>

                </div>


            </div>
        </div>
        <div class="tab-pane panel-body " id="tab4">
            <div class="page-content-wrap">
                <div class="container-fluid">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="panel-body">
                                <form name="form_busqueda_local" id="form_busqueda_local" method="post" action="">
                                    <div class="col-md-12 form-group">

                                        <div class="col-md-4">
                                            <label class="control-label">Nombre</label>
                                            <input id="nombre_local" class="form-control input" type="text" name="nombre_local"
                                                   placeholder="Nombre" value="<?php echo $nombre; ?>"
                                                   class="input-small"/>
                                        </div>

                                        <div class="col-md-4">
                                            <label class="control-label">Dirección</label>
                                            <input id="direccion_local" class="form-control input" type="text" name="direccion_local"
                                                   placeholder="Dirección" value="<?php echo $direccion; ?>"
                                                   class="input-small"/>
                                        </div>


                                        <div class="col-md-4">
                                            <label class="control-label">Estado</label>
                                            <select class="form-control select" name="estado_local" id="estado_local">
                                                <option value="">Seleccione</option>
                                                <option value="1" <?php if ($estado == "1") echo "selected"; ?>>Activo
                                                </option>
                                                <option value="0" <?php if ($estado == "0") echo "selected"; ?>>
                                                    Inactivo
                                                </option>
                                            </select>

                                        </div>


                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-4">

                                        </div>
                                        <div class="col-md-8">
                                            <!--  <input type="submit" class="btn btn-primary pull-right" value="Buscar"
                                                    id="buscar_foto"/>-->
                                            <a href="#" class="btn btn-primary pull-right" id="buscar_empresa_local">Buscar</a>
                                        </div>

                                    </div>

                                </form>


                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Locales</h3>
                        <a   id="export_local" class="btn btn-danger dropdown-toggle pull-right"  style="margin-left: 5px;" ><img src="<?php echo base_url('assets/backend/');?>/img/icons/xls.png" width="16"/> Exportar</a>

                        <a href="<?php echo site_url($this->config->item('path_backend') . '/Local_Empresa/agregar/' . $empresa->id); ?>"
                           class="btn btn-default pull-right"><i class="fa fa-plus"></i> Agregar</a>

                    </div>
                    <div id="tabla_empresa_local"></div>

                </div>


            </div>
        </div>
    </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
    <script type="text/javascript">

        var jvalidate = $("#editar_empresa").validate({
            ignore: [],
            rules: {
                nombre: {
                    required: true
                },
                razon_social: {
                    required: true

                },

                formula: {
                    required: true

                },
                estado: {
                    required: true
                }
            }
        });
        $(document).ready(function(){


            $('#ruc').keypress(validateNumber);


        });

        function validateNumber(event) {
            var key = window.event ? event.keyCode : event.which;

            if (event.keyCode === 8 || event.keyCode === 46
                || event.keyCode === 37 || event.keyCode === 39) {
                return true;
            }
            else if ( key < 48 || key > 57 ) {
                return false;
            }
            else return true;
        }

        $("#lista_empresa_contacto").click(function () {

            $.post("<?php echo site_url('backend/Contacto/listar');?>", {id_empresa : <?php echo $empresa->id;?>},
                function (result) {

                    $('#tabla_empresa_contacto').html(result);


                }, "html");

            return true;
        });

        $("#buscar_empresa_contacto").click(function () {


            $.post("<?php echo site_url('backend/Contacto/buscar');?>",
                {id_empresa : <?php echo $empresa->id;?> , nombre :$("#nombre_contacto").val() ,
                    apellido : $("#apellido_contacto").val(),dni : $("#dni_contacto").val(),estado : $("#estado_contacto").val()},
                function (result) {
                    $('#tabla_empresa_contacto').html(result);

                }, "html");

            return true;
        });


        $("#lista_empresa_banner").click(function () {

            $.post("<?php echo site_url('backend/Banner/listar');?>",
                {id_empresa : <?php echo $empresa->id;?>},
                function (result) {

                    $('#tabla_empresa_banner').html(result);


                }, "html");

            return true;
        });

        $("#buscar_empresa_banner").click(function () {


            $.post("<?php echo site_url('backend/Banner/buscar');?>",
                {id_empresa : <?php echo $empresa->id;?> , nombre :$("#nombre_banner").val() ,
                    tipo : $("#tipo_banner").val(),estado : $("#estado_banner").val()},
                function (result) {
                    $('#tabla_empresa_banner').html(result);

                }, "html");

            return true;
        });

        $("#lista_empresa_local").click(function () {

            $.post("<?php echo site_url('backend/Local_empresa/listar');?>",  {id_empresa : <?php echo $empresa->id;?>},
                function (result) {

                    $('#tabla_empresa_local').html(result);


                }, "html");

            return true;
        });

        $("#buscar_empresa_local").click(function () {


            $.post("<?php echo site_url('backend/Local_empresa/buscar');?>",
                {id_empresa : <?php echo $empresa->id;?> , nombre :$("#nombre_local").val() ,
                    direccion : $("#direccion_local").val(),estado : $("#estado_local").val()},
                function (result) {
                    $('#tabla_empresa_local').html(result);

                }, "html");

            return true;
        });





        <?php
        switch($this->uri->segment(5)){
        case 'C' :
        ?>
        $(document).ready(function () {
            $('#lista_empresa_contacto').trigger('click');
        });
        <?php  break;
        case 'B' :
        ?>
        $(document).ready(function () {
            $('#lista_empresa_banner').trigger('click');
        });
        <?php  break;
        case 'L' :
        ?>
        $(document).ready(function () {
            $('#lista_empresa_local').trigger('click');
        });
        <?php
        break;
        }
        ?>
        
        

        $('#export_contacto').click(function(){
           //  e.preventDefault();

            var url = "<?php echo site_url($this->config->item('path_backend').'/Contacto/export');?>";
            var id_empresa = <?php echo $empresa->id;?>;
            var nombre = $("#nombre_contacto").val();
            var apellido = $("#apellido_contacto").val();
            var dni = $("#dni_contacto").val();
            var estado = $("#estado_contacto").val();

            window.location.href= url+"?id_empresa="+id_empresa+"&nombre="+nombre+"&apellido="+apellido+"&dni="+dni+"&estado="+estado;


        });

        $('#export_banner').click(function(){
            // e.preventDefault();

            var url = "<?php echo site_url($this->config->item('path_backend').'/Banner/export');?>";
            var id_empresa = <?php echo $empresa->id;?>;
            var nombre=$("#nombre_banner").val();
            var tipo=$("#tipo_banner").val();
            var estado=$("#estado_banner").val();

            window.location.href= url+"?id_empresa="+id_empresa+"&nombre="+nombre+"&tipo="+tipo+"&estado="+estado;


        });

        $('#export_local').click(function(){
            // e.preventDefault();

            var url = "<?php echo site_url($this->config->item('path_backend').'/Local_Empresa/export');?>";
            var id_empresa = <?php echo $empresa->id;?>;
            var nombre=$("#nombre_local").val();
            var direccion=$("#direccion_local").val();
            var estado=$("#estado_local").val();

            window.location.href= url+"?id_empresa="+id_empresa+"&nombre="+nombre+"&direccion="+direccion+"&estado="+estado;


        });

     
    </script>
